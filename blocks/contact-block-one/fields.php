<?php

use IMSWP\Helper\Fields;

$fields = new Fields(__DIR__);


$fields->register_tab(
    "Address",
    [
        ["Title", "text"],
        ["Content", "wysiwyg"],
    ]
);

$fields->register_tab(
    "Get In Touch",
    [
        ["Title", "text"],
        ["Phone", "link"],
        ["Mail", "link"],
    ]
);

$fields->register_tab(
    "Business Hours",
    [
        ["Title", "text"],
        ["Content", "wysiwyg"],
    ]
);

$fields->register_tab(
    "Content",
    [
        ["Title", "text"],
        ["Subtitle", "text"],
        ["Form ID", "text"],
    ]
);