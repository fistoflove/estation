<?php

use IMSWP\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "Cards",
    [
        ["Items", "repeater", [
            ["Image", "image"],
            ["Title", "text"],
            ["Content", "wysiwyg"],
            ["Button", "link"],
        ]],
        ["Button", "link"],
    ]
);