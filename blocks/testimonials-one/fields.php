<?php

use IMSWP\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "Testimonials",
    [
        ["Items", "repeater", [
            ["Image", "image"],
            ["Content", "wysiwyg"],
            ["Author", "text"],
        ]]
    ]
);