<?php

$example_field = get_field( 'example_field' );

?>

<section class="blog-feed <?= blockClasses( $block ) ?>" <?= blockSection( $block ) ?>>
	<div class="container-sm">
		<div class="wrapper">
			<list class="cards blog">
				<?php
				$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
				$args = array(
					'post_type'          => 'post',
					'posts_per_page'     => 8,
					'ignore_sticky_posts' => 0,
					'paged'              => $paged,
				);

				$my_query = new WP_Query($args);

				while ($my_query->have_posts()) : $my_query->the_post();
				?>
				<item class="card"><a class="card-container" href="<?php echo get_permalink(); ?>">
						<div class="card-image">
							<img src="<?php echo get_the_post_thumbnail_url()?>" alt="<?php the_title(); ?>"/>
						</div>
						<div class="card-body">
							<div class="card-type hide-desktop">
								<span>Blog</span>
							</div>
							<div class="card-info">
								<div class="related-content">
									<h3><?php the_title(); ?></h3>
								</div>
							</div>
							<div class="card-sum">
								<p class="preview"><?php echo wp_trim_words(get_the_content(), 15, '...');?></p>
							</div>
							<div class="card-footer">
								<span class="hide-mobile"><strong>Blog</strong> | </span><label><?php echo get_the_date('d.m.Y'); ?></label>
							</div>
						</div>
					</a></item>
				<?php endwhile; ?>
			</list>
			<?php
			if (function_exists('paginate_links')) :
				?>
				<div class="pagination">
					<?php
					echo paginate_links(array(
						'total' => $my_query->max_num_pages,
						'prev_text' => false,
						'next_text' => false
					));
					?>
				</div>
			<?php endif;

			wp_reset_postdata();
			?>
		</div>
	</div>
</section>
<style>
	.blog-feed .cards{
		max-width: 1200px;
		margin: 0 auto;
	}
.pagination{
	margin-top: 50px;
	display: flex;
	justify-content: center;
	gap: 10px;
}
.page-numbers{
	font-size: 21px;
}
@media screen and (max-width: 1024px){
	.page-numbers{
		font-size: 18px;
	}
}
</style>
