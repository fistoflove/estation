<?php

use Timber\Timber;
use Timber\Post;
use IMSWP\Helper\Scss;

include_once("fields.php");

$context = Timber::context();

$context['post'] = new Post(get_the_ID());

$context['block'] = $block;

$context['fields'] = get_fields();

$context['is_preview'] = $is_preview;

$args = [
    'post_type' => 'case-study',
    'posts_per_page' => 8,
    'ignore_sticky_posts' => 0,
    'order' => 'date'
];

$context['objects'] = Timber::get_posts( $args );

Timber::render( 'template.twig', $context);