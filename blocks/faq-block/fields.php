<?php

use IMSWP\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "Questions",
    [
        ["Items", "repeater", [
            ["Question", "text"],
            ["Answer", "wysiwyg"]
        ]]
    ]
);