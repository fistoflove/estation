<?php

use IMSWP\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "Cards",
    [
        ["Items", "repeater", [
            ["Title", "text"],
            ["Content", "wysiwyg"]
        ]],
    ]
);