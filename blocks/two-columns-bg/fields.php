<?php

use IMSWP\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "Content",
    [
        ["First Column", "wysiwyg"],
        ["Second Column", "wysiwyg"],
    ]
);