<?php

use IMSWP\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "First",
    [
        ["Title", "text"],
        ["Image", "image"],
        ["Content", "wysiwyg"],
        ["Button", "link"],
    ]
);

$fields->register_tab(
    "Second",
    [
        ["Title", "text"],
        ["Image", "image"],
        ["Content", "wysiwyg"],
        ["Button", "link"],
    ]
);
